#!/bin/sh
INFILE=${0%/*}/../../mrpeach-meta.pd
OUTDIR=${0%/*}/../extra

mkdir -p "${OUTDIR}"
for i in xbee slip
do
 cat "${INFILE}" | sed -e "s|NAME .*;|NAME $i;|g" > "${OUTDIR}/${i}-meta.pd"
done
