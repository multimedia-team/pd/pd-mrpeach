#!/bin/sh

resultfile=$(mktemp)
flags="-noprefs -nosound -nomidi -nogui -nrt"

pd -version ${flags}
runtest() {
    if ! pd ${flags} -send "runtest 1" "$1"; then
      echo "$1 FAILED" >> "${resultfile}"
    fi
}

for f in "$@"; do
    echo "testing ${f}"
    runtest "${f}"
done

result=0
if [ -s "${resultfile}" ]; then
  cat "${resultfile}" 1>&2
  result=1
fi
rm -f "${resultfile}"
exit "${result}"
